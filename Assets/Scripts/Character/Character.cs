﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class Character : MonoBehaviour
{
    public static event UnityAction<CharacterState> Event;
    public static Character Instance;

    private void Awake()
    {
        Instance = this;
    }

    public void SetEvent(CharacterState state)
    {
        Event?.Invoke(state);
    }
}

[System.Serializable]
public enum CharacterState{
    none,
    move,
    shoting,
    deadh
}
