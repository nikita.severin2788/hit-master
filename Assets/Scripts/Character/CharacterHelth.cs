﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterHelth : MonoBehaviour, IDamage
{

    [SerializeField] private int helthPoint;

    public void ApplyDamage(int helthPoint)
    {
        if (this.helthPoint > helthPoint)
            this.helthPoint -= helthPoint;
        else
            Character.Instance.SetEvent(CharacterState.deadh);
    }

}
