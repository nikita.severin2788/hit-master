﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMoving : MonoBehaviour
{

    [SerializeField] private PathPool path;
    [SerializeField] private Rigidbody RB;
    [SerializeField] private float Speed;
    private Transform targetPoint;
    private Animator characterAnimator;
    private int pointIdex = 0;

    private void Start()
    {
        if (GetComponent<Rigidbody>() != null) RB = GetComponent<Rigidbody>();

        if (GetComponent<Animator>() != null) characterAnimator = GetComponent<Animator>();
    }

    public void OnEnable()
    {
        Character.Event += CheckEvent;
    }

    public void OnDisable()
    {
        Character.Event -= CheckEvent;
    }

    private void CheckEvent(CharacterState state)
    {
        if (state == CharacterState.move) StartCoroutine(Move());
    }

    IEnumerator Move()
    {
        path = PathManager.Instance.GetNextPath();

        if (path.PointsList != null)
        {
            targetPoint = path.PointsList[0];
            characterAnimator.SetTrigger("run");
        }
        else
        {
            StopCoroutine(Move());
        }

        while (true)
        {
            CheckPosition();

            transform.position = Vector3.MoveTowards(transform.position, new Vector3(targetPoint.position.x, RB.position.y, targetPoint.position.z), Speed * Time.fixedDeltaTime);

            Quaternion rotTarget = Quaternion.LookRotation(new Vector3(targetPoint.position.x, RB.position.y, targetPoint.position.z) - transform.position);
            transform.rotation = Quaternion.Lerp(transform.rotation, rotTarget, 5 * Time.fixedDeltaTime);
            yield return new WaitForFixedUpdate();
        }
    }


    private void CheckPosition()
    {

        if (Vector3.Distance(transform.position, targetPoint.position) < 1f)
        {
            if (pointIdex < path.PointsList.Count-1)
            {
                pointIdex++;
                targetPoint.position = path.PointsList[pointIdex].position;
            }
            else
            {
                Character.Instance.SetEvent(CharacterState.shoting);
                characterAnimator.SetTrigger("idle");
                StopCoroutine(Move());
            }
        }
    }
}
