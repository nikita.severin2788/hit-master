﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterShooting : MonoBehaviour
{
    [SerializeField] private bool isAvailable = false;

    public static CharacterShooting Instance;

    public Transform ShotingPoint;
    public GameObject Bullet;

    private void Awake()
    {
        Instance = this;
    }

    public void OnEnable()
    {
        Character.Event += CheckEvent;
    }

    public void OnDisable()
    {
        Character.Event -= CheckEvent;
    }

    private void CheckEvent(CharacterState state)
    {
        if (state == CharacterState.shoting)
            isAvailable = true;
        else
            isAvailable = false;
    }

    public void SetRotation(Vector3 cursorPosition)
    {
        Quaternion rotTarget = Quaternion.LookRotation(new Vector3(cursorPosition.x, transform.position.y, cursorPosition.z) - transform.position);
        transform.rotation = Quaternion.Lerp(transform.rotation, rotTarget, 5 * Time.fixedDeltaTime);
        
    }

    public void Aiming(Vector3 cursorPosition)
    {
        transform.rotation = Quaternion.LookRotation(new Vector3(cursorPosition.x, transform.position.y, cursorPosition.z) - transform.position);


    }

    public void Shoot()
    {
        if (isAvailable)
        {
            GameObject bulletObject = Instantiate(Bullet,ShotingPoint);
            bulletObject.transform.position = ShotingPoint.transform.position;
            bulletObject.transform.SetParent(null);
            bulletObject.GetComponent<BulletScript>().AddForce();
        }
    }
}
