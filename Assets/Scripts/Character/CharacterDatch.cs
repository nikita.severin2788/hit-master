﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterDatch : MonoBehaviour
{
    
    [SerializeField] private Animator characterAnimator;

    private void Awake()
    {
        if (GetComponent<Animator>() != null)
            characterAnimator = GetComponent<Animator>();
    }

    public void OnEnable()
    {
        Character.Event += Death;
    }

    public void OnDisable()
    {
        Character.Event -= Death;
    }

    private void Death(CharacterState state)
    {
        if(state == CharacterState.deadh)
        {
            characterAnimator.SetTrigger("Deadh");
            Debug.Log("dead");
        }
    }


}
