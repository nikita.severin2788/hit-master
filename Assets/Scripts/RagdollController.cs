﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollController : MonoBehaviour
{
    [SerializeField]private Animator Animator;
    [SerializeField]private Transform[] ragdolls;
    [SerializeField] private Collider mainCollider;
    [SerializeField] private Rigidbody mainRgitbody;

    private void Awake()
    {
        for(int i = 0; i < ragdolls.Length; i++)
        {
            ragdolls[i].GetComponent<Collider>().enabled = false;
            ragdolls[i].GetComponent<Rigidbody>().isKinematic = false;
        }
    }


    public void Enable()
    {
        Animator.enabled = false;
        mainCollider.enabled = false;
        mainRgitbody.isKinematic = true;
        for (int i = 0; i < ragdolls.Length; i++)
        {
            ragdolls[i].GetComponent<Collider>().enabled = true;
            ragdolls[i].GetComponent<Rigidbody>().isKinematic = false;
        }
    }
}
