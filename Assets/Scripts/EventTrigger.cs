﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventTrigger : MonoBehaviour
{

    public string NameTag = " ";
    public UnityEvent OnEnter;
    public UnityEvent OnExit;
    public UnityEvent OnStay;


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(NameTag))
        {
            OnEnter.Invoke();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(NameTag))
        {

            OnExit.Invoke();
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag(NameTag))
        {

            OnStay.Invoke();
        }
    }

}
