﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmosPathRenderer : MonoBehaviour
{

    private Transform previousPoint;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawCube(transform.position, new Vector3(1, 1, 1));


        for (int i = 0; i < GetComponentsInChildren<Transform>().Length - 1; i++)
        {
            Transform point = transform.GetChild(i).GetComponent<Transform>();

            if (point != null && point != transform)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawSphere(point.position, 0.25f);

                if (previousPoint != null)
                    Gizmos.DrawLine(previousPoint.position, point.position);


                previousPoint = point.transform;
            }
        }

        previousPoint = null;
    }
}
