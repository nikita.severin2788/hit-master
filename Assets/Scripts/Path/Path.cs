﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path : MonoBehaviour
{

    [SerializeField] private PathPool pool;


    private void Awake()
    {
        foreach(Transform point in GetComponentsInChildren<Transform>())
        {
           if(point != transform) pool.PointsList.Add(point);
        }
    }

    private void Start()
    {
        if (PathManager.Instance != null) PathManager.Instance.AddPath(pool);
    }
}
