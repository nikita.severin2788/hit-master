﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathManager : MonoBehaviour
{

    [SerializeField] private List<PathPool> PathList = new List<PathPool>();
    [SerializeField] private int setPathIndex = 0;

    public static PathManager Instance;

    private void Awake()
    {
        if (PathManager.Instance == null)
            Instance = this;
    }

    public void AddPath(PathPool pathPool)
    {
        bool isCoinsided = false;

        foreach(PathPool pool in PathList)
        {
            if (pool.PathIndex == pathPool.PathIndex)
            {
                isCoinsided = true;

            }
        }
        if (!isCoinsided) PathList.Add(pathPool);
    }

    public PathPool GetPath(int pathIndex)
    {
        foreach (PathPool pool in PathList)
        {
            if (pool.PathIndex == pathIndex)
            {
                return pool;
            }
            break;
        }
        return null;
    }
    public PathPool GetNextPath()
    {
        if (setPathIndex < PathList.Count)
        {
            foreach (PathPool setPath in PathList)
            {
                if(setPath.PathIndex == setPathIndex)
                {
                    setPathIndex++;
                    return setPath;
                }
            }
        }
        return null;
    }
}
