﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PathPool
{
    public int PathIndex;
    public List<Transform> PointsList = new List<Transform>();
    
    public PathPool (int pathIndex, List<Transform> pointList)
    {
        PathIndex = pathIndex;
        PointsList = pointList;
    }
}
