﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Action : MonoBehaviour
{

    [SerializeField] private CharacterState state;

    public void ActionEvent()
    {
        Character.Instance.SetEvent(state);
    }
}
