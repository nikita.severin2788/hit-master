﻿using UnityEngine;

public class CameraRig : MonoBehaviour
{
    [SerializeField] private Vector3 rigOffset;
    [SerializeField] private float speed;
    [SerializeField] private Transform characterTransform;

    private void Start()
    {
        if (Character.Instance != null)
        {
            characterTransform = Character.Instance.GetComponent<Transform>();
        }
    }

    void FixedUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, characterTransform.position + rigOffset, speed * Time.deltaTime);
    }
}
