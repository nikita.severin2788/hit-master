﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouthController : MonoBehaviour, IPointerDownHandler
{
    [SerializeField] private bool isAvailable = false;

    public void OnEnable()
    {
        Character.Event += CheckEvent;
    }

    public void OnDisable()
    {
        Character.Event -= CheckEvent;
    }

    private void CheckEvent(CharacterState state)
    {
        if (state == CharacterState.shoting)
            isAvailable = true;
        else
            isAvailable = false;

    }

    public void Update()
    {
        if (isAvailable)
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
            {
                CharacterShooting.Instance.SetRotation(hit.point);
                CharacterShooting.Instance.Aiming(hit.point);
            }
        }
    }


    public void OnPointerDown(PointerEventData eventData)
    {
        if (isAvailable)
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
            {
                CharacterShooting.Instance.Shoot();
            }
        }
    }
}
