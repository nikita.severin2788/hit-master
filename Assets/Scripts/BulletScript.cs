﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    [SerializeField] private Rigidbody RB;
    [SerializeField] private float ImpulseValue;
    [SerializeField] private int damageValue;

    [SerializeField] private float destroyDistance;
    private Vector3 startPostion;

    public void AddForce()
    {
        RB.AddForce(transform.forward * ImpulseValue, ForceMode.Impulse);
    }

    private void Start()
    {
        startPostion = transform.position;
    }

    private void Update()
    {
        if(Vector3.Distance(startPostion, transform.position) > destroyDistance)
        {
            Destroy(gameObject);

        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.GetComponent<IDamage>() != null) 
            collision.transform.GetComponent<IDamage>().ApplyDamage(damageValue);

        Destroy(gameObject);
    }


}
