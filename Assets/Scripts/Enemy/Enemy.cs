﻿using UnityEngine;

public class Enemy : MonoBehaviour
{

    public delegate void PublicsEvent(EnemyState state);
    public event PublicsEvent Event;


    public void SetEvent(EnemyState state)
    {
        Event(state);
    }
}