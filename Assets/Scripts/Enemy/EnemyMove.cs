﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMove : MonoBehaviour
{
    [SerializeField] private NavMeshAgent navAgent;
    [SerializeField] private Animator enemyAnimator;
    [SerializeField] private Enemy enemy;

    private void Awake()
    {
        if (GetComponent<Enemy>() != null) enemy = GetComponent<Enemy>();

        if (GetComponent<NavMeshAgent>()!= null) navAgent = GetComponent<NavMeshAgent>();

        if (GetComponent<Animator>() != null) enemyAnimator = GetComponent<Animator>();
    }

    public void OnEnable()
    {
        if (enemy != null) enemy.Event += CheckEvent;
    }

    public void OnDisable()
    {
        if (enemy != null) enemy.Event += CheckEvent;
    }

    private void CheckEvent(EnemyState state)
    {
        if (state == EnemyState.walk) Move(Character.Instance.transform);
        if (state == EnemyState.none) Stop();
    }

    private void Move(Transform target)
    {
        if(navAgent != null)
        {
            navAgent.SetDestination(target.position);

            if (enemyAnimator != null) enemyAnimator.SetTrigger("walk");
        }
    }

    private void Stop()
    {
        enemyAnimator.SetTrigger("idle");
        navAgent.Stop();
        enemy.GetComponent<Rigidbody>().isKinematic = true;
    }

}
