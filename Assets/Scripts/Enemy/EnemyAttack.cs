﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{

    [SerializeField] private Enemy enemy;
    [SerializeField] private int damageValue = 101;
    [SerializeField] private bool isAvailable = false;

    public void Awake()
    {
        if (GetComponent<Enemy>() != null) enemy = GetComponent<Enemy>();
    }

    public void OnEnable()
    {
        if (enemy != null) enemy.Event += CheckEvent;
    }

    public void OnDisable()
    {
        if (enemy != null) enemy.Event -= CheckEvent;
    }

    private void CheckEvent(EnemyState state)
    {
        if (state == EnemyState.walk)
            isAvailable = true;
        else
            isAvailable = false;
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.GetComponent<Character>() != null)
            Attack(collision.transform);
    }

    private void Attack(Transform character)
    {
        if (isAvailable)
        {
            if (character.GetComponent<IDamage>() != null)
            {
                enemy.SetEvent(EnemyState.attack);

                if (GetComponent<Animator>() != null) 
                    GetComponent<Animator>().SetTrigger("attack");

                character.GetComponent<IDamage>().ApplyDamage(damageValue);
                StartCoroutine(Delay());
            }
        }
    }

    private IEnumerator Delay()
    {
        yield return new WaitForSeconds(1f);
        enemy.SetEvent(EnemyState.none);
    }



}
