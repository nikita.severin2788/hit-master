﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHelth : MonoBehaviour, IDamage
{
    public GameObject helthPointPrefab;
    public delegate void PublicEvent(int helthPoint);
    public event PublicEvent HelthUpdate;

    

    [SerializeField] private int helthPoint;
    [SerializeField] private Enemy enemy;

    public void Start()
    {
        if (GetComponent<Enemy>() != null) enemy = GetComponent<Enemy>();
    }


    public void SpawnHelthLine()
    {
        if (helthPointPrefab != null)
        {
            GameObject SetObject = Instantiate(helthPointPrefab, CanvasSingleton.Instance.transform);
            SetObject.GetComponent<HelthLineUI>().Parent = transform;
        }
    }

    public void ApplyDamage(int helthPoint)
    {
        if (this.helthPoint > helthPoint)
        {
            this.helthPoint -= helthPoint;
        }
        else
        {
            helthPoint = 0;
            enemy.SetEvent(EnemyState.death);
        }
        HelthUpdate(helthPoint);
    }
}
