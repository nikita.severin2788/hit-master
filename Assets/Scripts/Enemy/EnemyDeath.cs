﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeath : MonoBehaviour
{

    [SerializeField] private Enemy enemy;

    public void Awake()
    {
        if (GetComponent<Enemy>() != null) enemy = GetComponent<Enemy>();
        if (enemy != null) enemy.Event += CheckEvent;
    }

    public void OnEnable()
    {
        if (enemy != null) enemy.Event += CheckEvent;
    }

    public void OnDisable()
    {
        if (enemy != null) enemy.Event -= CheckEvent;
    }

    private void CheckEvent(EnemyState state)
    {
        if (state == EnemyState.death) Death();
    }

    public void Death()
    {
        if (GetComponent<RagdollController>() != null)
            GetComponent<RagdollController>().Enable();
    }
}
