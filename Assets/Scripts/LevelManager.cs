﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [SerializeField] private int enemyCount;

    [SerializeField] private int delayInSeconds;

    [SerializeField] List<Enemy> enemys = new List<Enemy>();

    [SerializeField] private bool isActivate = false;

    public void Action()
    {
        if (!isActivate)
        {
            StartCoroutine(EnemyCheckUpdate());
            isActivate = true;
        }
    }

    private IEnumerator EnemyCheckUpdate()
    {
        yield return new WaitForSeconds(delayInSeconds);
        EnemyActivate();
        while (true)
        {
            if (enemys.Count == 0)
            {
                Character.Instance.SetEvent(CharacterState.move);
                break;
            }
            yield return new WaitForEndOfFrame();
        }
    }

    private void EnemyActivate()
    {
        if (enemys.Count > 0)
        {
            foreach (Enemy setEnemy in enemys)
            {
                setEnemy.SetEvent(EnemyState.walk);
                setEnemy.GetComponent<EnemyHelth>().SpawnHelthLine();
            }
        }
    }
}
