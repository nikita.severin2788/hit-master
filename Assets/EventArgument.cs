﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EventArgument : EventArgs
{

    public EnemyState State;

    public EventArgument(EnemyState state)
    {
        State = state;
    }

}
