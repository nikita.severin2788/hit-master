﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelthLineUI : MonoBehaviour
{
    
    
    [SerializeField] private Image Line;
    public Transform Parent;
    [SerializeField] private float uiOffset;

    private void OnEnable()
    {
        if (Parent != null)
        {
            if (Parent.GetComponent<EnemyHelth>() != null)
            {
                Parent.GetComponent<EnemyHelth>().HelthUpdate += SetLine;
                Parent.GetComponent<EnemyHelth>().HelthUpdate += LineDestroy;
            }
        }
    }
    private void OnDisable()
    {
        if (Parent.GetComponent<EnemyHelth>() != null){
            Parent.GetComponent<EnemyHelth>().HelthUpdate -= SetLine;
            Parent.GetComponent<EnemyHelth>().HelthUpdate -= LineDestroy;
        }
    }


    public void SetLine(int poins)
    {
        Line.fillAmount = poins / 100;
    }

    private void LineDestroy(int points)
    {
        if (points == 0) Destroy(gameObject);
    }

    private void LateUpdate()
    {
        transform.position = Camera.main.WorldToScreenPoint(new Vector3(Parent.position.x,
                                                                        Parent.position.y + uiOffset,
                                                                        Parent.position.z));
    }


}
